var path = require('path');
var express = require('express');
var app = express();
var http = require('http').Server(app);
// var io = require('socket.io')(http);
var io = require('socket.io')(http, {origins:'localhost:* http://localhost:* generic.dev:* http://generic.dev:*'});

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'node_modules')));

app.get('/', function(req, res){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
  
  console.log("A user has connected");
  io.emit('chat message', {for:'everyone', "message": "A new user has connected!"});
  socket.emit('chat message', {for:'oneperson', "message": "Welcome you, whoever you are!"});

  socket.on('chat message', function(msg){
  	console.log("Someone send message", msg);
    io.emit('chat message', msg);
  });

   socket.on('disconnect', function(){
    console.log('user disconnected');
    io.emit('chat message', {for:'everyone', "message": "A new user has disconnected!"});
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});
