var ExpoAction = require('../actions/ExpoAction');
var $ = jQuery = require('jquery');

module.exports = {

  // Load mock product data from localStorage into ProductStore via Action
  getEventData: function(callback) {
    // var data = JSON.parse(localStorage.getItem('events'));
    
    var jqxhr = $.ajax("./data/event.json")
	  .done(function(data) {
	  	ExpoAction.receiveEvents(data);
	  	callback();
	  })
	  .fail(function(event) {
	    console.log( "error", event );
	  })
	  .always(function() {
	  });
  },

  getPeopleData: function(callback) {
  	var jqxhr = $.ajax("./data/people.json")
	  .done(function(data) {
	  	ExpoAction.receivePeople(data);
	  	callback();
	  })
	  .fail(function(event) {
	    console.log( "error", event );
	  })
	  .always(function() {
	  });
  },

  getTimelineData: function(callback){
    var jqxhr = $.ajax("./data/timeline.json")
	  .done(function(data) {
	  	ExpoAction.receiveTimeline(data);
	  	callback();
	  })
	  .fail(function(event) {
	    console.log( "error", event );
	  })
	  .always(function() {
	  });
  }
};