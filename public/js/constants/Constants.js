var keyMirror = require('react/lib/keyMirror');

// Define action constants
module.exports = keyMirror({
  RECEIVE_MESSAGE: null,
  SEND_MESSAGE: null
});
