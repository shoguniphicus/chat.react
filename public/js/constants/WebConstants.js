var url = require('url');
var parsed = url.parse(location.href);

var exportObject = new Object();

switch(parsed.hostname){
	case "generic.dev":
		exportObject.image_url = "/leo/expo/img";
	break;

	case "mustardworldwide.com":
		exportObject.image_url = "/dev/pmo/expo/img";
	break;

	case "localhost":
		exportObject.image_url = "/arcww/pmo/milano/img";
	break;

	case "apps-stg.malaysia.my":
		exportObject.image_url = "/expomilano2015/img";
	break;

	default:
		exportObject.image_url = "img";
	break;
}

module.exports = {
  IMAGE_URL:exportObject.image_url
};