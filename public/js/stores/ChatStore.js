var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var Constants = require('../constants/Constants');
var _ = require('underscore');

// Define initial data points
var _chats = [];

function addMessage(data){
  _chats.push(data);
}

// Extend ProductStore with EventEmitter to add eventing capabilities
var ChatStore = _.extend({}, EventEmitter.prototype, {
  getMessages:function(){
    return _chats;
  },

  // Emit Change event
  emitChange: function() {
    this.emit('change');
  },

  // Add change listener
  addChangeListener: function(callback) {
    this.on('change', callback);
  },

  // Remove change listener
  removeChangeListener: function(callback) {
    this.removeListener('change', callback);
  }

});

// Register callback with AppDispatcher
AppDispatcher.register(function(payload) {
  var action = payload.action;
  var text;

  switch(action.actionType) {
    case Constants.RECEIVE_MESSAGE:
      addMessage(action.data);
    break;

    default:
    return true;
  }

  // If action was responded to, emit change event
  ChatStore.emitChange();

  return true;

});

module.exports = ChatStore;
