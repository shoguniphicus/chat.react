var React = require('react');
var $ = jQuery = require('jquery');
var io = require('socket.io-client');
var socket = io('http://localhost:3000');
var ChatStore = require('../stores/ChatStore');
var ExpoAction = require('../actions/ExpoAction');
var ChatCell = require('./cell/Chat.react');

// Method to retrieve state from Stores
function getStates() {
  return {
    messageTyped: "",
    chats : ChatStore.getMessages()
    // months: EventStore.getEvents()
  };
}

function executeTest(){

}

// Define main Controller View
var ChatApp = React.createClass({
  // Get initial state from stores
  getInitialState: function() {
    return getStates();
  },

  // Add change listeners to stores
  componentDidMount: function() {
    var context = this;

    ChatStore.addChangeListener(this._onChange);

    socket.on('connect', function(){
      // console.log("Connected");
    });

    socket.on('chat message', function(data){
      // console.log("Chat Message", data);
      ExpoAction.receiveMessage(data);
    });

    socket.on('disconnect', function(){
      console.log("DC!");
    });

    $('form').submit(function(event){
      event.preventDefault();

      context.onHandleClick();
      context.state.messageTyped = "";
    });
  },

  // Remove change listers from stores
  componentWillUnmount: function() {
    ChatStore.removeChangeListener(this._onChange);
  },

  onHandleClick: function(){
    socket.emit('chat message', {for:'everyone', message: this.state.messageTyped});
  },

  onHandleChange: function(event){
    this.setState({messageTyped: event.target.value});
  },

  // Render our child components, passing state via props
  render: function() {
  	return (
      <div className="app">
        <div className="container">
          <div className="row">
            <form actions="">
              <input className="col-xs-12" onChange={this.onHandleChange} value={this.state.messageTyped} />
              <div className="btn btn-default col-xs-4" onClick={this.onHandleClick}>
                Send Message
              </div>
            </form>
          </div>
          <div className="row">
            <ul>
              {this.state.chats.map(function(single, index){
                return(
                    <ChatCell message={single} key={index} />
                  )
              })}
            </ul>
          </div>
        </div>
      </div>
  	);
  },

  // <SuggestApp visible={this.state.suggestVisible} />

  // Method to setState based upon Store changes
  _onChange: function() {
    this.setState(getStates());
  }

});

module.exports = ChatApp;