var React = require('react');
var $ = jQuery = require('jquery');

// Method to retrieve state from Stores
function getStates() {
  return {
    // months: EventStore.getEvents()
  };
}

function executeTest(){

}

// Define main Controller View
var SpeedtestApp = React.createClass({
  // Get initial state from stores
  getInitialState: function() {
    return getStates();
  },

  // Add change listeners to stores
  componentDidMount: function() {
  },

  // Remove change listers from stores
  componentWillUnmount: function() {
  },

  upload: function(){
    var p;var canvas = document.createElement("canvas");
    var img1= new Image();
    img1.src = "./img/ss.png"; 

    function getBase64Image(_img){     
        canvas.width = img1.width; 
        canvas.height = img1.height; 
        var ctx = canvas.getContext("2d"); 
        ctx.drawImage(_img, 0, 0); 
        var dataURL = canvas.toDataURL("image/png");

        //alert("from getbase64 function"+dataURL );    
        return dataURL;
    } 

    var base64 = getBase64Image(img1)+getBase64Image(img1)+getBase64Image(img1)+getBase64Image(img1)+getBase64Image(img1);

    var posturlfile = "http://staging.nextsecureapps.com/st/php/upload.php";
    // var posturlfile = "./php/upload.php";

    var dataPackage = new Object();
    dataPackage.payload = base64;

    $(".b64").html(base64);
    console.log("Byte Size", byteCount(base64));

    function byteCount(s) {
        return encodeURI(s).split(/%..|./).length - 1;
    }

    $.ajax({
      xhr: function() {
        var xhr = new window.XMLHttpRequest();

        xhr.upload.addEventListener("progress", function(evt) {
          if (evt.lengthComputable) {
            var percentComplete = evt.loaded / evt.total;
            percentComplete = parseInt(percentComplete * 100);
            console.log(percentComplete);

            if (percentComplete === 100) {

            }

          }
        }, false);

        return xhr;
      },
      url: posturlfile,
      type: "POST",
      data: JSON.stringify(dataPackage),
      cache: false,
      processData: false,
      // enctype: 'multipart/form-data',
      // contentType: "application/octet-stream",
      success: function(result) {
        console.log(result);
      }
    });
  },

  // Render our child components, passing state via props
  render: function() {
  	return (
      <div className="app">
        <div className="btn btn-default" onClick={this.upload}>UPLOAD</div>
        <div className="container">
          <div className="b64"></div>
        </div>
      </div>
  	);
  },

  // <SuggestApp visible={this.state.suggestVisible} />

  // Method to setState based upon Store changes
  _onChange: function() {
    this.setState(getStates());
  }

});

module.exports = SpeedtestApp;