var React = require('react');
var ExpoAction = require('../actions/ExpoAction');
var EventCell = require('./cell/Event.react');
var EventMonthCell = require('./cell/EventMonth.react');
var _ = require('underscore');
var $ = jQuery = require('jquery');
var owl = null;
var currentMonthItemCount = 0;

function doOwl(){
  // $("#owl-month").owlCarousel({
  //    autoPlay: false, //Set AutoPlay to 3 seconds
  //    items : 6,
  //    responsive: true
  //  });
  
  var leftControl = $(".event-nav-left");
  var rightControl = $(".event-nav-right");

  $("#owl-event").owlCarousel({
    autoPlay: false,
    items: 3,
    itemsCustom : [
        [0, 1],
        [450, 1],
        [600, 1],
        [700, 2],
        [1000, 3],
        [1200, 3],
        [1400, 3],
        [1600, 3]
      ],
      navigation : false,
      afterAction: function(){
        if(owl == null)
          return;

        // if(owl.currentItem == 0){
        //   leftControl.addClass('disabled');
        // }else{
        //   leftControl.removeClass('disabled');
        // }

        // if((owl.currentItem + 1) == currentMonthItemCount){
        //   rightControl.addClass('disabled');
        // }else{
        //   rightControl.removeClass('disabled');
        // }
      }
  });

  owl = $('#owl-event').data('owlCarousel');
}

// Flux product view
var EventApp = React.createClass({

  navLeft: function(){
    owl.prev();
  },

  navRight: function(){
    owl.next();
  },

  componentDidMount: function() {
    $( document ).ready(function() {
      // Handler for .ready() called.
      doOwl();
    });
  },

  // Remove change listers from stores
  componentWillUnmount: function() {
    $('.modal').off();
  },

  componentWillUpdate: function(){
    //var owl = $('#owl-event').data('owlCarousel');
    owl.destroy();
  },

  componentDidUpdate: function(){
    doOwl();
  },

  // Render product View
  render: function() {
    var allMonths = this.props.months;
    var selectedMonthData = this.props.selected;

    currentMonthItemCount = selectedMonthData.events.length;

    return (
      <div id="events" className="container-fluid bg-yellow">
        <div className="row">
          <div className="col-xs-12 component-block">
            <div className="col-xs-12 col-md-10 col-md-push-1 copy top30 bigasscopy">
              <div className="title babyblue">
                The Events
              </div>
              <div className="col-xs-12 col-md-8 col-md-push-2 subheader grey">
                184 days of events, spread over 2,047 square metres, the Malaysia Pavilion is full of dazzling multi-cultural happenings. 
              </div>
              <div className="col-lg-push-2 col-lg-8 top30">
                <div className="col-xs-12" id="owl-month" onClick={this.clickFuck}>
                  {this.props.months.map(function(single, index){
                    return(
                        <EventMonthCell key={index} data={single} selected={selectedMonthData} />
                      )
                  })}
                </div>
              </div>
              <div className="col-xs-12">
                <div className="col-xs-12 col-md-push-2 col-md-8" id="owl-event">
                  {selectedMonthData.events.map(function(single, index){
                    return(
                        <EventCell key={index} data={single} selected={selectedMonthData}/>
                      )
                  })}
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xs-12 event-nav-control">
                <div className="pull-left">
                  <div className="btn event-nav-left" onClick={this.navLeft}></div>
                </div>
                <div className="pull-right">
                  <div className="btn event-nav-right" onClick={this.navRight}></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  },

});

module.exports = EventApp;