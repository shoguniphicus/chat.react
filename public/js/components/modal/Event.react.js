var React = require('react');
var ExpoAction = require('../../actions/ExpoAction');
var $ = jQuery = require('jquery');
var bootstrap = require('bootstrap');

// Flux product view
var EventModal = React.createClass({
  // Render product View

  componentWillReceiveProps: function(nextProps) {
  	 if(nextProps.visible){
	 	$(".event-modal").modal("show");
	 }else{
	    $(".event-modal").modal("hide");
	 }
	},

  componentDidMount: function() {
  	$('.event-modal').on('hidden.bs.modal', function (e) {
    	ExpoAction.updateEventModalVisible(false);
	})
  },

  // Remove change listers from stores
  componentWillUnmount: function() {
  	$('.modal').off();
  },

  render: function() {
    var setting = this.props.selected;

    return (
      <div className="modal fade event-modal">
		  <div className="modal-dialog">
		    <div className="modal-content">
		      <div className="modal-header">
		        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 className="modal-title">{setting.title}</h4>
		      </div>
		      <div className="modal-body">
		        <p>{setting.des}</p>
		      </div>
		      <div className="modal-footer">
		      	<p>{setting.subheader}</p>
		      </div>
		    </div>
		  </div>
		</div>
    );
  },

});

module.exports = EventModal;