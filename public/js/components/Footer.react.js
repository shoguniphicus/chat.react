var React = require('react');
var ExpoAction = require('../actions/ExpoAction');

// Flux view
var FooterApp = React.createClass({
  // Render view
  render: function() {
  	return (
      <div className="container-fluid override-container-fluid">
			<div className="row">
				<div className="col-xs-12 footerbg">
					<div className="footer-inner">
						<div className="col-xs-12 col-lg-10">
							<span>Copyright 2015 Malaysia Pavillion at Expo Milano 2015</span>
						</div>
						<div className="col-xs-12 col-lg-2 social-container">
							<div className="pull-right">
								<span>Follow us on:</span>
								<ul className="social">
									<li><a href="https://www.facebook.com/MalaysiaPavilion" target="_blank"><span className="fb"></span></a></li>
									<li><a href="https://twitter.com/Msia_Pavilion" target="_blank"><span className="tw"></span></a></li>
									<li><a href="https://instagram.com/malaysiapavilion2015/" target="_blank"><span className="ig"></span></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    );
  }
});
module.exports = FooterApp;