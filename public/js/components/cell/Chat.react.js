var React = require('react');
var ExpoAction = require('../../actions/ExpoAction');
var WebConstants = require('../../constants/WebConstants');

var ChatCell = React.createClass({
  render: function() {
    var setting = this.props.message;
    return (
      <li>
        {setting.message}
      </li>
    );
  },

});

module.exports = ChatCell;