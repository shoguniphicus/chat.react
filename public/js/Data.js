var localStorage = require('localStorage');
var $ = jQuery = require('jquery');

module.exports = {
  // Load Mock Product Data Into localStorage
  init: function() {
    localStorage.clear();

    // this.initEventData();
    // this.initPeopleData();
  },
  initEventData:function(callback){
    localStorage.setItem('events', JSON.stringify(eventData));
  },
  initPeopleData:function(){
    localStorage.setItem('people', JSON.stringify(peopleData));
  },
  initTimelineData:function(){
    localStorage.setItem('timeline', JSON.stringify(timelineData));
  }

};