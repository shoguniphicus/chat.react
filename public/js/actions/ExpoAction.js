var AppDispatcher = require('../dispatcher/AppDispatcher');
var Constants = require('../constants/Constants');

// Define action methods
var ExpoActions = {

  // Events
  receiveMessage: function(data) {
    AppDispatcher.handleAction({
      actionType: Constants.RECEIVE_MESSAGE,
      data: data
    })
  },

  sendMessage: function(data){
    AppDispatcher.handleAction({
      actionType: Constants.SEND_MESSAGE,
      data: data
    })
  }
};

module.exports = ExpoActions;
